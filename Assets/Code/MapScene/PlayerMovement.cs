﻿using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    public MasterGraph NavigationGrid;
    public float SpeedFactor;
    public float DistanceToNav;
    public int EncounterChance;

    private bool isMoving;
    private RouteNode endTarget;
    private RouteNode currentTarget;
    private Route route;

    void Start()
    {
        if (PlayerNavData.PlayerPosition != Vector3.zero)
        {
            this.transform.position = PlayerNavData.PlayerPosition;
        }
    }

    void FixedUpdate ()
    {
        if (isMoving)
        {
            Move();
        }
	}

    void Move()
    {
        if (currentTarget != null)
        {
            this.transform.Translate((currentTarget.transform.position - this.transform.position).normalized * SpeedFactor);

            if (Vector3.Distance(this.transform.position, currentTarget.transform.position) < DistanceToNav)
            {
                this.transform.position = currentTarget.transform.position;
                if (currentTarget == endTarget)
                {
                    isMoving = false;
                }
                else
                {
                    currentTarget = route.FindNextNode(currentTarget.transform);
                }

                var encounterRoll = Random.Range(0, 100);
                if (encounterRoll < EncounterChance)
                {
                    this.isMoving = false;
                    SavePlayerProgress();
                    GameObject.Find("FadeScreen").GetComponent<SceneTransition>().SpiralOut("Combat");
                }
            }
        }
    }

    public void SetTarget(RouteNode target)
    {
        route = new Route(Pathfinding.FindBestPath(this.NavigationGrid.FindClosestNode(this.transform), target,
            this.NavigationGrid.Graph).ToArray());
        currentTarget = route.FirstNode;
        endTarget = route.LastNode;
        isMoving = true;
    }

    public void SavePlayerProgress()
    {
        PlayerNavData.PlayerPosition = this.transform.position;
    }
}
