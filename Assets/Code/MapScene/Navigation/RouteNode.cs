﻿using System.Collections.Generic;
using UnityEngine;

public class RouteNode : MonoBehaviour
{
    public List<RouteNode> Neighbours;
    public GameObject LocationRef;
    public GameObject NodePrefab;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        if (Neighbours != null)
        {
            foreach (var n in Neighbours)
            {
                Gizmos.DrawLine(this.transform.position, n.transform.position);
            }
        }
    }

    public void BuildNewNode()
    {
        var newNode = Instantiate(NodePrefab, this.transform.position + (Vector3.up*20), Quaternion.identity);
        newNode.transform.SetParent(this.transform.parent);
        newNode.name = this.name;
        var nodeElement = newNode.GetComponent<RouteNode>();
        this.Neighbours.Add(nodeElement);
        nodeElement.Neighbours = new List<RouteNode>();
        nodeElement.Neighbours.Add(this);
    }
}
