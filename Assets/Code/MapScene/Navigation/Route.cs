﻿using System.Linq;
using UnityEngine;

public class Route
{
    public RouteNode[] Nodes;

    public RouteNode LastNode
    {
        get { return Nodes[Nodes.Length - 1]; }
    }

    public RouteNode FirstNode
    {
        get { return Nodes.First(); }
    }

    public Route(RouteNode[] path)
    {
        Nodes = path;
    }

    public RouteNode FindNextNode(Transform current)
    {
        var node = Nodes.First(x => x.transform == current);
        return Nodes[Nodes.ToList().IndexOf(node) + 1];
    }
}
