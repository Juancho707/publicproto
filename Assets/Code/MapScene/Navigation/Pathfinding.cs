﻿using System.Collections.Generic;
using System.Linq;

public class Pathfinding
{
    public static IEnumerable<RouteNode> FindBestPath(RouteNode source, RouteNode target, RouteNode[] graph)
    {
        var dist = new Dictionary<RouteNode, int>();
        var prev = new Dictionary<RouteNode, RouteNode>();
        var unvisited = new List<RouteNode>();

        dist[source] = 0;
        prev[source] = null;

        foreach (var v in graph)
        {
            if (v != source)
            {
                dist[v] = int.MaxValue;
                prev[v] = null;
            }

            unvisited.Add(v);
        }

        while (unvisited.Any())
        {
            RouteNode u = null;

            foreach (var un in unvisited)
            {
                if (u == null || dist[un] < dist[u])
                {
                    u = un;
                }
            }

            if (u == target)
            {
                break;
            }

            unvisited.Remove(u);

            foreach (var n in u.Neighbours)
            {
                var alt = dist[u] + 1;
                if (alt < dist[n])
                {
                    dist[n] = alt;
                    prev[n] = u;
                }
            }
        }

        if (prev[target] != null)
        {
            var path = new List<RouteNode>();
            var node = target;
            while (node != null)
            {
                path.Add(node);
                node = prev[node];
            }

            path.Reverse();
            return path;
        }

        return null;
    }
}
