﻿using UnityEngine;

public class MasterGraph : MonoBehaviour
{
    public RouteNode[] Graph;

    public RouteNode GetRandomNode
    {
        get { return Graph.PickOne(); }
    }

    void Start()
    {
        Graph = this.GetComponentsInChildren<RouteNode>();
    }

    public RouteNode FindClosestNode(Transform origin)
    {
        float min = Vector3.Distance(origin.position, Graph[0].transform.position);
        RouteNode result = Graph[0];
        foreach (var n in Graph)
        {
            var dist = Vector3.Distance(origin.position, n.transform.position);
            if (dist < min)
            {
                min = dist;
                result = n;
            }
        }

        return result;
    }

    public void CheckGraphIntegrity()
    {
        var graph = this.GetComponentsInChildren<RouteNode>();
        foreach (var n in graph)
        {
            foreach (var n2 in n.Neighbours)
            {
                if (!n2.Neighbours.Contains(n))
                {
                    Debug.Log(string.Format("Discrepancy found on node {0} with node {1}", n2.name, n.name));
                }
            }
        }
    }
}
