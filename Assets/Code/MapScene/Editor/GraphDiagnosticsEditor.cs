﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MasterGraph))]
public class GraphDiagnosticsEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        MasterGraph myScript = (MasterGraph) target;
        if (GUILayout.Button("Check graph integrity"))
        {
            myScript.CheckGraphIntegrity();
        }
    }
}
