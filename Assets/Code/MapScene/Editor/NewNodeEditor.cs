﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(RouteNode))]
public class NewNodeEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        RouteNode myScript = (RouteNode) target;
        if (GUILayout.Button("Build new Node"))
        {
            myScript.BuildNewNode();
        }
    }
}
