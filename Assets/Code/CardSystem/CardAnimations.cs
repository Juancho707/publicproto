﻿using UnityEngine;

public class CardAnimations : MonoBehaviour
{
    public float MoveSpeed;
    public float DistanceToTargetWarp;

    private bool isRetired;
    private float retireSpeed = 30f;
    private float retireTime = 1f;
    private bool isMoving;
    private Transform moveToTarget;

    void FixedUpdate()
    {
        if (isMoving && moveToTarget != null)
        {
            this.transform.position = Vector3.Lerp(this.transform.position, moveToTarget.position, MoveSpeed);
            if (Vector3.Distance(this.transform.position, moveToTarget.position) < DistanceToTargetWarp)
            {
                ReachDestination();
            }
        }

        if (isRetired)
        {
            retireTime -= Time.fixedDeltaTime;
            this.transform.Translate(Vector3.left * retireSpeed);
            if (retireTime < 0)
            {
                Destroy(this.gameObject);
            }
        }
    }

    void ReachDestination()
    {
        this.transform.SetParent(moveToTarget);
        isMoving = false;
        this.transform.localPosition = Vector3.zero;
    }

    public void MoveTo(Transform target)
    {
        moveToTarget = target;
        this.transform.SetParent(Global.UIParent);
        isMoving = true;
    }

    public void RetireCard()
    {
        this.transform.SetParent(Global.UIParent);
        isRetired = true;
    }
}
