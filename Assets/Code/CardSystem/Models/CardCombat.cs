﻿using UnityEngine;

public class CardCombat : CardBase
{
    private bool isInPlay;

    public override void ClickCard()
    {
        isInPlay = !isInPlay;
        var gameZone = GameObject.Find("PlayerGame").transform;
        var handZone = GameObject.Find("PlayerHand").transform;

        if (isInPlay)
        {
            cardAnimator.MoveTo(gameZone);
        }
        else
        {
            cardAnimator.MoveTo(handZone);
        }
    }

    public void CalculateCombatStrength(Pillar enemyCardType)
    {
        //if (Data.CardType == enemyCardType)
        //{
        //    CombatStrength = Data.Strength;
        //}
        //else
        //{
        //    CombatStrength = Data.Strength / 2;
        //}

        CombatStrength = Data.Strength;
        PowerLbl.text = CombatStrength.ToString();
    }
}
