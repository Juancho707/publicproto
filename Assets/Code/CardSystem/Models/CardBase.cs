﻿using UnityEngine;
using UnityEngine.UI;

public abstract class CardBase : MonoBehaviour
{
    public CardData Data;
    public Text CaptionLbl;
    public Text TitleLbl;
    public Text PowerLbl;
    public Image Icon;
    public int CombatStrength;

    protected CardAnimations cardAnimator;

    protected virtual void Start()
    {
        cardAnimator = this.GetComponent<CardAnimations>();
    }

    public void UpdateCardInfo()
    {
        CombatStrength = Data.Strength;
        TitleLbl.text = Data.Title;
        PowerLbl.text = CombatStrength.ToString();
        Icon.sprite = Global.Icons.GetIcon(Data.CardType);
        Icon.color = Global.Icons.GetColor(Data.CardType);

        if (CaptionLbl != null)
        {
            CaptionLbl.text = Data.Text;
        }
    }

    public void UpdateCardInfo(CardData card)
    {
        this.Data = card;
        UpdateCardInfo();
    }

    public virtual void ClickCard()
    {
    }

    public void RetireCard()
    {
        this.cardAnimator.RetireCard();
    }

    public void MoveTo(Transform target)
    {
        if (cardAnimator == null)
        {
            cardAnimator = this.GetComponent<CardAnimations>();
        }

        cardAnimator.MoveTo(target);
    }
}
