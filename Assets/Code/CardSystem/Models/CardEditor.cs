﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardEditor : CardBase, IPointerEnterHandler, IPointerExitHandler
{
    private CardZoomed zoomedCard;

    protected override void Start()
    {
        base.Start();
        zoomedCard = GameObject.Find("ZoomedCard").GetComponent<CardZoomed>();
    }

    public override void ClickCard()
    {
        if (Data.IsOnDeck == false && PlayerPersonality.ActiveDeck.Count >= PlayerPersonality.MaxDeckSize)
        {

        }
        else
        {
            Data.IsOnDeck = !Data.IsOnDeck;
            var deckZone = GameObject.Find("Deck").transform;
            var reserveZone = GameObject.Find("Reserves").transform;

            if (Data.IsOnDeck)
            {
                PlayerPersonality.AvailableCards.Remove(Data);
                PlayerPersonality.ActiveDeck.Add(Data);
                cardAnimator.MoveTo(deckZone);
            }
            else
            {
                PlayerPersonality.ActiveDeck.Remove(Data);
                PlayerPersonality.AvailableCards.Add(Data);
                cardAnimator.MoveTo(reserveZone);
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        zoomedCard.Show();
        zoomedCard.UpdateCardInfo(this.Data);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        zoomedCard.Hide();
    }
}
