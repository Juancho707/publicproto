﻿using System;

[Serializable]
public class CardData
{
    public Pillar CardType;
    public int Strength;
    public int Rarity;
    public string Title;
    public string Text;
    public bool IsOnDeck;
    
    public static CardData CreateRandom()
    {
        var result = new CardData();
        result.CardType = (Pillar)UnityEngine.Random.Range(0, 3);
        result.Strength = UnityEngine.Random.Range(1, 9);
        
        switch (result.CardType)
        {
            case Pillar.Aggression:
                result.Title = "Mild aggression";
                result.Text = "Fuck you";
                break;
            case Pillar.Emotional:
                result.Title = "Empathical observation";
                result.Text = "Self respect by definition is a confidence and pride in knowing that your behaviour is both honorable and dignified. Respect yourself by respecting others. \n - Miya Yamanouchi";
                break;
            case Pillar.Rational:
                result.Title = "Deep thought";
                result.Text = "Something smart";
                break;
        }

        return result;
    }

    public static CardData CreateEnemyRandom()
    {
        var result = new CardData();
        result.CardType = (Pillar)UnityEngine.Random.Range(0, 3);
        result.Strength = UnityEngine.Random.Range(1, 12);
        switch (result.CardType)
        {
            case Pillar.Aggression:
                result.Title = "Mild aggression";
                result.Text = "Daaaaaamn!";
                break;
            case Pillar.Emotional:
                result.Title = "Insecurity attack";
                result.Text = "Lookin' for some love babe?";
                break;
            case Pillar.Rational:
                result.Title = "Chauvinistic observation";
                result.Text = "Are you allowed to go out like that?";
                break;
        }

        return result;
    }
}
