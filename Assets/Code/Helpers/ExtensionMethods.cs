﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class ExtensionMethods
{
    public static T PickOne<T>(this IEnumerable<T> col)
    {
        return col.ToArray()[Random.Range(0, col.Count())];
    }

    public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> col)
    {
        var shuffled = new List<T>();
        var deck = col.ToList();
        var deckCount = deck.Count();

        for (int i = 0; i < deckCount; i++)
        {
            var randomCard = deck.PickOne();
            shuffled.Add(randomCard);
            deck.Remove(randomCard);
        }

        return shuffled;
    }
}

