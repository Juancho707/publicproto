﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class DynamicLabel : MonoBehaviour
{
    public string Format;
    
    public void SetLabel(params object[] value)
    {
        this.GetComponent<Text>().text = string.Format(Format, value);
    }
}
