﻿using UnityEngine;

public class IconReferences : MonoBehaviour
{
    public Sprite RationalSp;
    public Sprite EmotionalSp;
    public Sprite AggressiveSp;

    public Color RationalCo;
    public Color EmotionalCo;
    public Color AggressiveCo;

    public Sprite GetIcon(Pillar cat)
    {
        switch (cat)
        {
            case Pillar.Aggression:
                return AggressiveSp;
            case Pillar.Emotional:
                return EmotionalSp;
            case Pillar.Rational:
                return RationalSp;
        }

        return RationalSp;
    }

    public Color GetColor(Pillar cat)
    {
        switch (cat)
        {
            case Pillar.Aggression:
                return AggressiveCo;
            case Pillar.Emotional:
                return EmotionalCo;
            case Pillar.Rational:
                return RationalCo;
        }

        return RationalCo;
    }
}
