﻿using UnityEngine;

public static class Global
{
    public static IconReferences Icons
    {
        get { return GameObject.Find("References").GetComponent<IconReferences>(); }
    }

    public static Transform UIParent
    {
        get { return GameObject.Find("UI").transform; }
    }

    public static Transform LooseCards
    {
        get { return GameObject.Find("LooseCards").transform; }
    }
}
