﻿using UnityEngine;
using UnityEngine.SceneManagement;

public static class SceneChanger
{
    public static void ChangeScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }
}
