﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneTransition : MonoBehaviour
{
    public float FadeSpeed;
    public float SpiralSpeed;
    public Transition StartTransition;

    private Image myImg;
    private bool isFading;
    private bool isSpiraling;

    private float fadeFactor;
    private float spiralFactor;
    private string nextScene;

    private void Start()
    {
        myImg = this.GetComponent<Image>();
        SetTransition();
    }

    private void FixedUpdate()
    {
        if (isSpiraling)
        {
            Spiral();
        }

        if (isFading)
        {
            Fade();
        }
    }

    void Spiral()
    {
        myImg.fillAmount += spiralFactor;

        if (spiralFactor > 0f && myImg.fillAmount >= 1f)
        {
            isSpiraling = false;
            TransitionFinished();
        }

        if (spiralFactor < 0f && myImg.fillAmount <= 0f)
        {
            isSpiraling = false;
        }
    }

    void Fade()
    {
        var col = myImg.color;
        col.a += fadeFactor;
        myImg.color = col;

        if (fadeFactor > 0f && myImg.color.a >= 1f)
        {
            isFading = false;
            TransitionFinished();
        }

        if (fadeFactor < 0f && myImg.color.a <= 0f)
        {
            isFading = false;
        }
    }

    void SetTransition()
    {
        switch (StartTransition)
        {
            case Transition.Fade:
                FadeIn();
                break;
            case Transition.Spiral:
                SpiralIn();
                break;
        }
    }

    public void FadeIn()
    {
        nextScene = string.Empty;
        myImg.fillAmount = 1f;
        var col = myImg.color;
        col.a = 1f;
        myImg.color = col;
        fadeFactor = -FadeSpeed;

        isFading = true;
    }

    public void FadeOut(string scene)
    {
        nextScene = scene;
        myImg.fillAmount = 1f;
        var col = myImg.color;
        col.a = 0f;
        myImg.color = col;
        fadeFactor = FadeSpeed;

        isFading = true;
    }

    public void SpiralIn()
    {
        nextScene = string.Empty;
        myImg.fillAmount = 1f;
        var col = myImg.color;
        col.a = 1f;
        myImg.color = col;
        spiralFactor = -SpiralSpeed;

        isSpiraling = true;
    }

    public void SpiralOut(string scene)
    {
        nextScene = scene;
        myImg.fillAmount = 0f;
        var col = myImg.color;
        col.a = 1f;
        myImg.color = col;
        spiralFactor = SpiralSpeed;

        isSpiraling = true;
    }

    void TransitionFinished()
    {
        if (!string.IsNullOrEmpty(nextScene))
        {
            SceneChanger.ChangeScene(nextScene);
        }
    }

    void BringToFront()
    {
        this.transform.SetSiblingIndex(20);
    }
}
