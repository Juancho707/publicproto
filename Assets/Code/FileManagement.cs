﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using UnityEngine;

public static class FileManagement
{
    private static string filePath = Application.dataPath + @"/Data/";

    public static object OpenSaves<T>(string fileName)
    {
        CreateFile(fileName);
        object result = null;

        try
        {
            using (var fs = File.Open(filePath + fileName, FileMode.OpenOrCreate))
            {
                var serializer = new XmlSerializer(typeof(T));
                result = serializer.Deserialize(fs);
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
        }

        return result;
    }

    public static void SaveFile<T>(T saveData, string fileName)
    {
        using (var sw = new StreamWriter(filePath + fileName))
        {
            var serializer = new XmlSerializer(typeof(T));
            serializer.Serialize(sw, saveData);
            sw.Flush();
        }
    }

    private static void CreateFile(string fileName)
    {
        if (!Directory.Exists(filePath))
        {
            Directory.CreateDirectory(filePath);
        }

        if (!File.Exists(filePath + fileName))
        {
            var f = File.Create(filePath + fileName);
            f.Close();
        }
    }
}
