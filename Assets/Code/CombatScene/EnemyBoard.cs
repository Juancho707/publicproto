﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class EnemyBoard : BoardBase
{
    public Transform NextCardPosition;
    public Transform PlayedCardPosition;
    public CardData PlayCard
    {
        get { return cardIndex < Deck.Count ? Deck[cardIndex] : null; }
    }

    public int MinDeckSize;
    public int MaxDeckSize;

    void Start()
    {
        Deck = new List<CardData>();
        GenerateDeck();
        Deck = Deck.Shuffle().ToList();
        cardIndex = 0;

        HpLbl.text = Hitpoints.ToString();
        PlayTurn();
    }

    public override void PlayTurn()
    {
        var currentCard = PlayedCardPosition.GetComponentInChildren<CardEnemy>();
        var nextCard = NextCardPosition.GetComponentInChildren<CardEnemy>();

        if (currentCard != null)
        {
            currentCard.RetireCard();
            cardIndex++;

            if (nextCard != null)
            {
                nextCard.MoveTo(PlayedCardPosition);
                var nuCard = DrawCard(DeckPlace, cardIndex + 1);

                if (nuCard != null)
                {
                    nuCard.MoveTo(NextCardPosition);
                }
            }
        }
        else
        {
            var firstCard = DrawCard(DeckPlace, cardIndex);
            if (firstCard != null)
            {
                firstCard.MoveTo(PlayedCardPosition);
            }

            var secondCard = DrawCard(DeckPlace, cardIndex + 1);
            if (secondCard != null)
            {
                secondCard.MoveTo(NextCardPosition);
            }
        }

        var deckDisplay = (DeckSize - cardIndex - 2);
        DeckLbl.text = deckDisplay > 0 ? deckDisplay.ToString() : "0";
    }

    void GenerateDeck()
    {
        DeckSize = Random.Range(MinDeckSize, MaxDeckSize);
        for (int i = 0; i < DeckSize; i++)
        {
            var newCard = CardData.CreateEnemyRandom();
            newCard.IsOnDeck = true;
            Deck.Add(newCard);
        }
    }

    public bool IsEnemyBeaten()
    {
        return (Hitpoints <= 0 || (DeckSize - cardIndex) <= 1);
    }
}
