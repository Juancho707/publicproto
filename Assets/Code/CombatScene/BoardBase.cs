﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoardBase : MonoBehaviour
{
    public int Hitpoints;
    public int DeckSize;
    public Text HpLbl;
    public Text DeckLbl;

    public GameObject CardPrefab;
    public Transform DeckPlace;
    public HpBar HealthBar;

    protected List<CardData> Deck;
    public int cardIndex;

    public virtual void PlayTurn()
    {
        
    }

    protected CardBase DrawCard(Transform place, int index)
    {
        if (index < Deck.Count)
        {
            var dc = GameObject.Instantiate(CardPrefab, place).GetComponent<CardBase>();
            dc.Data = Deck[index];
            dc.UpdateCardInfo();
            return dc;
        }

        return null;
    }

    public void DealDamage(int damage)
    {
        Hitpoints -= damage;
        HpLbl.text = Hitpoints < 0 ? "0" : Hitpoints.ToString();
        HealthBar.UpdateHpBar();
    }
}
