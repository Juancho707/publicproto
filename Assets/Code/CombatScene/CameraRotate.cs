﻿using UnityEngine;

public class CameraRotate : MonoBehaviour
{
    public float RotationSpeed;

    void FixedUpdate () {
		this.transform.Rotate(Vector3.up * RotationSpeed);
	}
}
