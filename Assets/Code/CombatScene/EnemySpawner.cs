﻿using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public Transform EnemySpawn;
    public GameObject[] EnemyPfs;

    private void Start()
    {
        this.GetComponent<CharAnimator>().LoadEnemy(Instantiate(EnemyPfs.PickOne(), EnemySpawn).GetComponent<Animator>());
    }
}
