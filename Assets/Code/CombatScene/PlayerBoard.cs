﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerBoard : BoardBase
{
    public Transform PlayerHand;
    public Transform PlayerGame;
    public int HandSize;

    private EnemyBoard enemy;

    void Start()
    {
        enemy = GameObject.Find("EnemyBoard").GetComponent<EnemyBoard>();
        Deck = PlayerPersonality.ActiveDeck.Shuffle().ToList();
        DeckSize = Deck.Count;
        HpLbl.text = Hitpoints.ToString();
        DrawHand();
    }
    
    public void DrawHand()
    {
        var cardsToDraw = HandSize - PlayerHand.GetComponentsInChildren<CardCombat>().Length;
        var drawnCards = new List<CardCombat>();

        for (int i = 0; i < cardsToDraw; i++)
        {
            var card = DrawCard(DeckPlace, cardIndex) as CardCombat;

            if (card != null)
            {
                card.MoveTo(PlayerHand);
                cardIndex++;
                drawnCards.Add(card);
            }
        }

        UpdateContextValues(drawnCards);
        var deckDisplay = (DeckSize - cardIndex);
        DeckLbl.text = deckDisplay > 0 ? deckDisplay.ToString() : "0";
    }

    public override void PlayTurn()
    {
        var playedCards = PlayerGame.GetComponentsInChildren<CardCombat>();
        var playerAttack = playedCards.Sum(x => x.CombatStrength);
        var enemyCard = GameObject.Find("EnemyGame").GetComponentInChildren<CardEnemy>();
        var enemyAttack = enemyCard.CombatStrength;

        var net = playerAttack - enemyAttack;

        if (net >= 0)
        {
            if (playedCards.All(x => x.Data.CardType == enemyCard.Data.CardType))
            {
                enemy.DealDamage(Mathf.Abs(net));
            }
        }
        else
        {
            DealDamage(Mathf.Abs(net));
        }

        EndCombat();

        foreach (var pc in playedCards)
        {
            pc.RetireCard();
        }

        enemy.PlayTurn();
        DrawHand();
    }
    
    void UpdateContextValues(IEnumerable<CardCombat> drawnCards)
    {
        var enemyCardType = enemy.PlayCard != null ? enemy.PlayCard.CardType : Pillar.Aggression;

        foreach (var c in PlayerHand.GetComponentsInChildren<CardCombat>().Union(drawnCards))
        {
            c.CalculateCombatStrength(enemyCardType);
        }
    }

    void EndCombat()
    {
        if (Hitpoints <= 0 || (DeckSize - cardIndex) <= 0)
        {
            GameObject.Find("FadeScreen").GetComponent<SceneTransition>().SpiralOut("Map");
        }
        else if (enemy.IsEnemyBeaten())
        {
            PlayerPersonality.AvailableCards.Add(CardData.CreateRandom());
            GameObject.Find("FadeScreen").GetComponent<SceneTransition>().SpiralOut("Map");
        }
    }
}
