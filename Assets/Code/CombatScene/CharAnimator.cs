﻿using UnityEngine;

public class CharAnimator : MonoBehaviour
{
    public Animator PlayerAnimator;
    [HideInInspector] public Animator EnemyAnimator;

    public void LoadEnemy(Animator enemy)
    {
        EnemyAnimator = enemy;
    }

    public void Animate()
    {
        PlayRandomTaunt(5);
        PlayRandomReply(4);
    }

    public void PlayRandomTaunt(int tauntCount)
    {
        EnemyAnimator.SetBool("IsTaunting", true);
        EnemyAnimator.SetInteger("TauntIndex", Random.Range(0, tauntCount));
    }

    public void PlayRandomReply(int replyCount)
    {
        PlayerAnimator.SetBool("IsReplying", true);
        PlayerAnimator.SetInteger("ReplyIndex", Random.Range(0, replyCount));
    }
}
