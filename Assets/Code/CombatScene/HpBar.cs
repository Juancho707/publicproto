﻿using System.Linq;
using UnityEngine;

public class HpBar : MonoBehaviour
{
    public RectTransform DamageProjection;
    public Transform MyGame;
    public Transform EnemyGame;
    public float resizeFactor;
    public bool IsPlayer;

    private RectTransform myRect;
    private BoardBase board;
    private int sizePerHp;
    private int targetSize;
    private bool isResizing;

    private void Start()
    {
        board = GetComponentInParent<BoardBase>();
        myRect = this.GetComponent<RectTransform>();
        CalculateSizePerHp();
    }

    private void FixedUpdate()
    {
        ProjectDamage();

        if (isResizing)
        {
            var rect = myRect.sizeDelta;
            rect.x -= resizeFactor;
            myRect.sizeDelta = rect;

            if (myRect.sizeDelta.x <= targetSize)
            {
                rect = myRect.sizeDelta;
                rect.x = targetSize;
                myRect.sizeDelta = rect;
                isResizing = false;
            }
        }
    }

    void CalculateSizePerHp()
    {
        var width = myRect.sizeDelta.x;
        var hp = board.Hitpoints;

        sizePerHp = (int)width / hp;
    }

    void ProjectDamage()
    {
        var myCards = MyGame.GetComponentsInChildren<CardBase>();
        var enemyCards = EnemyGame.GetComponentsInChildren<CardBase>();

        var myAttack = myCards.Any() ? myCards.Sum(x => x.CombatStrength) : 0;
        var enemyAttack = enemyCards.Any() ? enemyCards.Sum(x => x.CombatStrength) : 0;

        var netAttack = enemyAttack - myAttack;

        if (netAttack < 0)
        {
            netAttack = 0;
        }
        else if(!IsPlayer)
        {
            if (enemyCards.Any(x => x.Data.CardType != myCards.First().Data.CardType))
            {
                netAttack = 0;
            }
        }

        if (netAttack > board.Hitpoints)
        {
            netAttack = board.Hitpoints;
        }

        var rect = DamageProjection.sizeDelta;
        rect.x = netAttack * sizePerHp;
        DamageProjection.sizeDelta = rect;
    }

    public void UpdateHpBar()
    {
        targetSize = board.Hitpoints * sizePerHp;
        isResizing = true;
    }
}
