﻿using UnityEngine;

public class CameraFocus : MonoBehaviour
{
    public float FocusSpeed;
    public Transform HomePosition;

    private Transform focusTarget;
    private Transform moveTarget;
    private CameraPan panner;
    private bool isMoving;

    private void Start()
    {
        panner = this.GetComponent<CameraPan>();
    }

    private void FixedUpdate()
    {
        if (isMoving)
        {
            this.transform.position = Vector3.Lerp(this.transform.position, moveTarget.position, FocusSpeed);

            if (focusTarget != null)
            {
                this.transform.LookAt(focusTarget);
            }

            panner.StabilizeZAxis();
        }
    }

    public void FocusOnTarget(EvidenceInteractor focus)
    {
        moveTarget = focus.CameraHoverPoint;
        focusTarget = focus.transform;
        isMoving = true;
        panner.TogglePan();
    }

    public void Return()
    {
        moveTarget = HomePosition;
        focusTarget = null;
        isMoving = true;
        panner.TogglePan();
    }
}