﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InvestigateUI : MonoBehaviour
{
    public BarFade TopBar;
    public BarFade BottomBar;

    public void Focus(string message1, string message2)
    {
        TopBar.FadeIn(message1);
        BottomBar.FadeIn(message2);
    }

    public void UnFocus()
    {
        TopBar.FadeOut();
        BottomBar.FadeOut();
    }
}
