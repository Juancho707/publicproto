﻿using UnityEngine;

public class EvidenceInteractor : MonoBehaviour
{
    public Transform CameraHoverPoint;
    public string Thought;
    public string Notification;
    public string EvidenceId;

    private bool isFocused;

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (isFocused)
            {
                Camera.main.GetComponent<CameraFocus>().Return();
                Global.UIParent.GetComponent<InvestigateUI>().UnFocus();
            }
            else
            {
                Camera.main.GetComponent<CameraFocus>().FocusOnTarget(this);
                Global.UIParent.GetComponent<InvestigateUI>().Focus(Thought, Notification);
            }

            isFocused = !isFocused;
        }
    }
}