﻿using UnityEngine;

public class CameraPan : MonoBehaviour
{
    public float RotationMultiplier;
    
    private Vector3 prevMousePos;
    private bool panEnabled = true;

    private void FixedUpdate()
    {
        if (panEnabled)
        {
            if (Input.GetMouseButton(0))
            {
                if (prevMousePos.x != Input.mousePosition.x)
                {
                    this.transform.Rotate(Vector3.up, (prevMousePos.x - Input.mousePosition.x) * RotationMultiplier);
                }

                if (prevMousePos.y != Input.mousePosition.y)
                {
                    this.transform.Rotate(-Vector3.right,
                        (prevMousePos.y - Input.mousePosition.y) * RotationMultiplier);
                }

                StabilizeZAxis();
            }
        }

        prevMousePos = Input.mousePosition;
    }

    public void TogglePan()
    {
        panEnabled = !panEnabled;
    }

    public void StabilizeZAxis()
    {
        var angles = this.transform.eulerAngles;
        angles.z = 0f;
        this.transform.eulerAngles = angles;
    }
}