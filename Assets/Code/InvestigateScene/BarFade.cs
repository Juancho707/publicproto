﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BarFade : MonoBehaviour
{
    public float FadeSpeed;

    private Image myImg;
    private bool isFading;
    private float fadeFactor;
    private string displayText;

    private void Start()
    {
        myImg = this.GetComponent<Image>();
    }

    private void FixedUpdate()
    {
        if (isFading)
        {
            Fade();
        }
    }

    void Fade()
    {
        var col = myImg.color;
        col.a += fadeFactor;
        myImg.color = col;

        if (fadeFactor > 0f && myImg.color.a >= 1f)
        {
            isFading = false;
            TransitionFinished();
        }

        if (fadeFactor < 0f && myImg.color.a <= 0f)
        {
            isFading = false;
            TransitionFinished();
        }
    }

    public void FadeOut()
    {
        displayText = string.Empty;
        UpdateMessageText();

        var col = myImg.color;
        col.a = 1f;
        myImg.color = col;
        fadeFactor = -FadeSpeed;

        isFading = true;
    }

    public void FadeIn(string text)
    {
        displayText = text;
        
        var col = myImg.color;
        col.a = 0f;
        myImg.color = col;
        fadeFactor = FadeSpeed;

        isFading = true;
    }

    void TransitionFinished()
    {
        UpdateMessageText();
    }

    void UpdateMessageText()
    {
        var text = this.GetComponentInChildren<Text>();

        if (text != null)
        {
            text.text = displayText;
        }
    }
}
