﻿using System.Collections.Generic;
using System.Linq;

public static class CardDatabase
{
    public static IEnumerable<CardData> PlayerCardDB;
    public static IEnumerable<CardData> EnemyCardDB;

    private static string playerDbFile = "PlayerCards.xml";
    private static string enemyDbFile = "EnemyCards.xml";

    public static void LoadDataBase()
    {
        PlayerCardDB = FileManagement.OpenSaves<IEnumerable<CardData>>(playerDbFile) as IEnumerable<CardData>;
        EnemyCardDB = FileManagement.OpenSaves<IEnumerable<CardData>>(enemyDbFile) as IEnumerable<CardData>;
    }

    public static CardData TakeRandomPlayerCard()
    {
        var pick = PlayerCardDB.PickOne();
        return CopyCardIntoNew(pick);
    }

    public static CardData TakeRandomPlayerCard(int rarity)
    {
        var pick = PlayerCardDB.Where(x=> x.Rarity == rarity).PickOne();
        return CopyCardIntoNew(pick);
    }

    public static CardData TakeRandomEnemyPlayerCard()
    {
        var pick = EnemyCardDB.PickOne();
        return CopyCardIntoNew(pick);
    }

    public static CardData TakeRandomEnemyPlayerCard(int rarity)
    {
        var pick = EnemyCardDB.Where(x => x.Rarity == rarity).PickOne();
        return CopyCardIntoNew(pick);
    }

    static CardData CopyCardIntoNew(CardData pick)
    {
        return new CardData
        {
            Strength = pick.Strength,
            CardType = pick.CardType,
            Rarity = pick.Rarity,
            Text = pick.Text,
            Title = pick.Title
        };
    }

    public static void SaveDummyData()
    {
        var pCards = new List<CardData>();
        var eCards = new List<CardData>();
        
        pCards.Add(CardData.CreateRandom());
        pCards.Add(CardData.CreateRandom());
        pCards.Add(CardData.CreateRandom());

        eCards.Add(CardData.CreateEnemyRandom());
        eCards.Add(CardData.CreateEnemyRandom());
        eCards.Add(CardData.CreateEnemyRandom());

        FileManagement.SaveFile(pCards, playerDbFile);
        FileManagement.SaveFile(eCards, enemyDbFile);
    }
}