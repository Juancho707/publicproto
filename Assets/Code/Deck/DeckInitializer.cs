﻿using UnityEngine;

public class DeckInitializer : MonoBehaviour
{
    public GameObject CardPrefab;

    void Start()
    {
        foreach (var c in PlayerPersonality.ActiveDeck)
        {
            var dc = GameObject.Instantiate(CardPrefab, this.transform).GetComponent<CardEditor>();
            dc.Data = c;
            dc.UpdateCardInfo();
        }
    }
}
