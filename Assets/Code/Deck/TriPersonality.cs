﻿using UnityEngine;
using UnityEngine.UI;

public class TriPersonality : MonoBehaviour
{
    public int Rational;
    public int Emotional;
    public int Aggressive;

    public Transform Point;
    public TrianglePoints Triangle;

    public Text AggLbl;
    public Text RatLbl;
    public Text EmoLbl;
    public DynamicLabel DeckCount;

    private float rationalPos;
    private float emotionalPos;
    private float aggressivePos;

    void Update()
    {
        UpdateDeckCount();
        UpdateValues();
        UpdateLabels();
        UpdateRatios();
        UpdatePointPos();
    }

    void UpdateValues()
    {
        Rational = PlayerPersonality.RationalValue;
        Emotional = PlayerPersonality.EmotionalValue;
        Aggressive = PlayerPersonality.AggresiveValue;
    }

    void UpdateRatios()
    {
        float total = Rational + Emotional + Aggressive;

        rationalPos = Rational / total;
        emotionalPos = Emotional / total;
        aggressivePos = Aggressive / total;
    }

    void UpdatePointPos()
    {
        var rationalBase = Vector3.Lerp(Triangle.AggressiveVert.position, Triangle.EmotionalVert.position, 0.5f);
        var emotionalBase = Vector3.Lerp(Triangle.AggressiveVert.position, Triangle.RationalVert.position, 0.5f);
        var aggressiveBase = Vector3.Lerp(Triangle.RationalVert.position, Triangle.EmotionalVert.position, 0.5f);

        var rationalValuePoint = Vector3.Lerp(rationalBase, Triangle.RationalVert.position, rationalPos);
        var emotionalValuePoint = Vector3.Lerp(emotionalBase, Triangle.EmotionalVert.position, emotionalPos);
        var aggressiveValuePoint = Vector3.Lerp(aggressiveBase, Triangle.AggressiveVert.position, aggressivePos);

        var triangleSide1 = Vector3.Lerp(rationalValuePoint, emotionalValuePoint, 0.5f);
        Point.position = Vector3.Lerp(triangleSide1, aggressiveValuePoint, 0.5f);
    }

    void UpdateLabels()
    {
        AggLbl.text = Aggressive.ToString();
        RatLbl.text = Rational.ToString();
        EmoLbl.text = Emotional.ToString();
    }

    void UpdateDeckCount()
    {
        DeckCount.SetLabel(PlayerPersonality.ActiveDeck.Count, PlayerPersonality.MaxDeckSize);
    }
}
