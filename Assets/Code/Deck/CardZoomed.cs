﻿using UnityEngine;
using UnityEngine.UI;

public class CardZoomed : CardBase
{
    public float FadeSpeed;
    public bool HasFadeEffect;

    private bool isFading;
    private float fadeFactor;
    private float solid = 1f;
    private float transparent = 0f;
    private Image background;

    private void Start()
    {
        background = this.GetComponent<Image>();
    }

    private void FixedUpdate()
    {
        if (isFading)
        {
            Fade();
            if (fadeFactor < 0f && background.color.a < 0f)
            {
                SetFadeTo(0f);
                isFading = false;
            }

            if (fadeFactor > 0f && background.color.a > 1f)
            {
                SetFadeTo(1f);
                isFading = false;
            }
        }
    }

    public override void ClickCard()
    {
    }

    public void Show()
    {
        if (HasFadeEffect)
        {
            isFading = true;
            fadeFactor = FadeSpeed;
        }
        else
        {
            var col = TitleLbl.color;
            col.a = solid;
            TitleLbl.color = col;

            col = CaptionLbl.color;
            col.a = solid;
            CaptionLbl.color = col;

            col = Icon.color;
            col.a = solid;
            Icon.color = col;

            col = PowerLbl.color;
            col.a = solid;
            PowerLbl.color = col;

            col = background.color;
            col.a = solid;
            background.color = col;
        }
    }

    public void Hide()
    {
        if (HasFadeEffect)
        {
            isFading = true;
            fadeFactor = -FadeSpeed;
        }
        else
        {
            var col = TitleLbl.color;
            col.a = transparent;
            TitleLbl.color = col;

            col = CaptionLbl.color;
            col.a = transparent;
            CaptionLbl.color = col;

            col = Icon.color;
            col.a = transparent;
            Icon.color = col;

            col = PowerLbl.color;
            col.a = transparent;
            PowerLbl.color = col;

            col = background.color;
            col.a = transparent;
            background.color = col;
        }
    }

    void Fade()
    {
        var col = TitleLbl.color;
        col.a += fadeFactor;
        TitleLbl.color = col;

        col = CaptionLbl.color;
        col.a += fadeFactor;
        CaptionLbl.color = col;

        col = Icon.color;
        col.a += fadeFactor;
        Icon.color = col;

        col = PowerLbl.color;
        col.a += fadeFactor;
        PowerLbl.color = col;

        col = background.color;
        col.a += fadeFactor;
        background.color = col;
    }

    void SetFadeTo(float fadeValue)
    {
        var col = TitleLbl.color;
        col.a = fadeValue;
        TitleLbl.color = col;

        col = CaptionLbl.color;
        col.a = fadeValue;
        CaptionLbl.color = col;

        col = Icon.color;
        col.a = fadeValue;
        Icon.color = col;

        col = PowerLbl.color;
        col.a = fadeValue;
        PowerLbl.color = col;

        col = background.color;
        col.a = fadeValue;
        background.color = col;
    }
}
