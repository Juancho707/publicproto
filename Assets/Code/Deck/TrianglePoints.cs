﻿using System;
using UnityEngine;

[Serializable]
public class TrianglePoints
{
    public Transform RationalVert;
    public Transform EmotionalVert;
    public Transform AggressiveVert;
}
