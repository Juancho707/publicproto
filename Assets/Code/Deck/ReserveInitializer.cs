﻿using UnityEngine;

public class ReserveInitializer : MonoBehaviour
{
    public GameObject CardPrefab;

    void Start()
    {
        foreach (var c in PlayerPersonality.AvailableCards)
        {
            var dc = GameObject.Instantiate(CardPrefab, this.transform).GetComponent<CardEditor>();
            dc.Data = c;
            dc.UpdateCardInfo();
        }
    }
}
