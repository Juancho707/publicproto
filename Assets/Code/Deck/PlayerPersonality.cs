﻿using System.Collections.Generic;
using System.Linq;

public static class PlayerPersonality
{
    public static int AggresiveValue
    {
        get { return ActiveDeck.Where(x => x.CardType == Pillar.Aggression).Sum(x => x.Strength); }
    }

    public static int RationalValue
    {
        get { return ActiveDeck.Where(x => x.CardType == Pillar.Rational).Sum(x => x.Strength); }
    }
    public static int EmotionalValue
    {
        get { return ActiveDeck.Where(x => x.CardType == Pillar.Emotional).Sum(x => x.Strength); }
    }

    private const int InitialDeckSize = 10;
    private const int InitialExtraCards = 4;

    public static int MaxDeckSize = 15;
    public static List<CardData> ActiveDeck;
    public static List<CardData> AvailableCards;

    static PlayerPersonality()
    {
        CardDatabase.SaveDummyData();
        ActiveDeck = new List<CardData>();
        AvailableCards = new List<CardData>();
        GenerateDeck();
    }

    static void GenerateDeck()
    {
        for (int i = 0; i < InitialDeckSize; i++)
        {
            var newCard = CardData.CreateRandom();
            newCard.IsOnDeck = true;
            ActiveDeck.Add(newCard);
        }

        for (int i = 0; i < InitialExtraCards; i++)
        {
            var newCard = CardData.CreateRandom();
            newCard.IsOnDeck = false;
            AvailableCards.Add(CardData.CreateRandom());
        }
    }
}
