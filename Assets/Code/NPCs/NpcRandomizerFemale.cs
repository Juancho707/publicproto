﻿using UnityEngine;

public class NpcRandomizerFemale : MonoBehaviour
{
    public int ShirtMatIndex;
    public int PantsMatIndex;
    public int SkintMatIndex;
    public int ShoesMatIndex;
    public int HairMatIndex;
    public SkinnedMeshRenderer BodyMesh;
    public SkinnedMeshRenderer HairMesh;

    private ResourceManager resources;

    void Start()
    {
        resources = GameObject.Find("GameManager").GetComponent<ResourceManager>();
        RandomizeMaterials();
    }

    void RandomizeMaterials()
    {
        var mats = this.BodyMesh.materials;
        mats[ShirtMatIndex] = resources.Shirts.PickOne();
        mats[PantsMatIndex] = resources.Pants.PickOne();
        mats[SkintMatIndex] = resources.Skins.PickOne();
        mats[ShoesMatIndex] = resources.Shoes.PickOne();

        var hairMats = this.HairMesh.materials;
        mats[HairMatIndex] = resources.Hairs.PickOne();

        this.BodyMesh.materials = mats;
        this.HairMesh.materials = hairMats;
    }
}

