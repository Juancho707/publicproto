﻿using UnityEngine;

public class NpcRandomizer : MonoBehaviour
{
    public int ShirtMatIndex;
    public int PantsMatIndex;
    public int SkintMatIndex;
    public int ShoesMatIndex;
    public int HairMatIndex;
    public SkinnedMeshRenderer Mesh;

    private ResourceManager resources;

    void Start()
    {
        resources = GameObject.Find("SceneManager").GetComponent<ResourceManager>();
        RandomizeMaterials();
    }

    void RandomizeMaterials()
    {
        var mats = this.Mesh.materials;
        mats[ShirtMatIndex] = resources.Shirts.PickOne();
        mats[PantsMatIndex] = resources.Pants.PickOne();
        mats[SkintMatIndex] = resources.Skins.PickOne();
        mats[ShoesMatIndex] = resources.Shoes.PickOne();
        mats[HairMatIndex] = resources.Hairs.PickOne();

        this.Mesh.materials = mats;
    }
}

