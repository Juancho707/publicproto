﻿using UnityEngine;

public class ResourceManager : MonoBehaviour
{
    public Material FriendlyColor;
    public Material NeutralColor;
    public Material EnemyColor;

    public Material[] Shirts;
    public Material[] Pants;
    public Material[] Skins;
    public Material[] Shoes;
    public Material[] Hairs;
}

